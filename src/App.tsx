import React from "react";

import ProjectFlow from "./components/ProjectFlow";
import OrgSelector from "./components/OrgSelector";

import { useStickyState } from "./hooks/useStickyState";

/*
 * A React component that contains the framework for the site.
 */
export default function App(): JSX.Element {
    const [orgs, setOrgs] = useStickyState<string[]>([], "saved-orgs");

    return (
        <div>
            <OrgSelector orgs={orgs} setOrgs={setOrgs} />
            <ProjectFlow orgs={orgs} />
        </div>
    );
}
