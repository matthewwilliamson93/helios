import { Dispatch, SetStateAction, useState, useEffect } from "react";

/*
 * A react hook that has its previous state backed up in local storage.
 *
 * When loading this hook from a new run, with history, it reloads the
 * value that was last stored.
 *
 * Args:
 *     defaultValue: The default state of the hook.
 *     key: The key to save the value to in local storage.
 *
 * Returns:
 *     The react hooks useState, type and Dispatch / SetStateAction.
 */
export function useStickyState<S>(
    defaultValue: S,
    key: string
): [S, Dispatch<SetStateAction<S>>] {
    const [value, setValue] = useState(() => {
        const stickyValue = window.localStorage.getItem(key);
        return stickyValue !== null ? JSON.parse(stickyValue) : defaultValue;
    });

    useEffect(() => {
        window.localStorage.setItem(key, JSON.stringify(value));
    }, [key, value]);

    return [value, setValue];
}
