import dagre from "dagre";

import React from "react";
import ReactFlow, {
    ConnectionLineType,
    Position,
    ReactFlowProvider,
    isNode,
} from "react-flow-renderer";

import { openInNewTab } from "../utils/browser";
import { listProjectCardsForOrg } from "../utils/githubApi";

const dagreGraph = new dagre.graphlib.Graph();
dagreGraph.setGraph({ rankdir: "LR" });
dagreGraph.setDefaultEdgeLabel(() => ({}));

const nodeWidth = 172;
const nodeHeight = 36;

function getLayoutedElements(elements) {
    elements.forEach((el) => {
        if (isNode(el)) {
            dagreGraph.setNode(el.id, { width: nodeWidth, height: nodeHeight });
        } else {
            dagreGraph.setEdge(el.source, el.target);
        }
    });

    dagre.layout(dagreGraph);

    return elements.map((el) => {
        if (isNode(el)) {
            const nodeWithPosition = dagreGraph.node(el.id);
            el.targetPosition = Position.Left;
            el.sourcePosition = Position.Right;
            el.position = {
                x: nodeWithPosition.x - nodeWidth / 2 + Math.random() / 1000,
                y: nodeWithPosition.y - nodeHeight / 2,
            };
        }

        return el;
    });
}

const position = { x: 0, y: 0 };
const edgeType = "smoothstep";

const initialElements = [
    {
        id: "1",
        type: "input",
        data: { label: "input" },
        position,
    },
    {
        id: "2",
        data: { label: "node 2" },
        position,
    },
    {
        id: "2a",
        data: { label: "node 2a" },
        position,
    },
    {
        id: "2b",
        data: { label: "node 2b" },
        position,
    },
    {
        id: "2c",
        data: { label: "node 2c" },
        position,
    },
    {
        id: "2d",
        data: { label: "node 2d" },
        position,
    },
    {
        id: "3",
        data: { label: "node 3" },
        position,
    },
    {
        id: "4",
        data: { label: "node 4" },
        position,
    },
    {
        id: "5",
        data: { label: "node 5" },
        position,
    },
    {
        id: "6",
        type: "output",
        data: { label: "output" },
        position,
    },
    { id: "7", type: "output", data: { label: "output" }, position },
    { id: "e12", source: "1", target: "2", type: edgeType, animated: true },
    { id: "e13", source: "1", target: "3", type: edgeType, animated: true },
    { id: "e22a", source: "2", target: "2a", type: edgeType, animated: true },
    { id: "e22b", source: "2", target: "2b", type: edgeType, animated: true },
    { id: "e22c", source: "2", target: "2c", type: edgeType, animated: true },
    { id: "e2c2d", source: "2c", target: "2d", type: edgeType, animated: true },
    { id: "e45", source: "4", target: "5", type: edgeType, animated: true },
    { id: "e56", source: "5", target: "6", type: edgeType, animated: true },
    { id: "e57", source: "5", target: "7", type: edgeType, animated: true },
];

/*
 * A React component that creates the flow diagram between the projects.
 *
 * https://github.com/wbkd/react-flow
 */
export default function ProjectFlow({ orgs }: { orgs: string[] }): JSX.Element {
    const layoutedElements = getLayoutedElements(initialElements);

    const onElementClick = (evt, id) => {
        evt.stopPropagation();
        if (id.data.url === undefined) {
            return;
        }

        openInNewTab(id.data.url);
    };

    return (
        <div>
            <ReactFlowProvider>
                <ReactFlow
                    elements={layoutedElements}
                    connectionLineType={ConnectionLineType.SmoothStep}
                    onElementClick={onElementClick}
                    snapToGrid={true}
                    snapGrid={[15, 15]}
                    style={{ width: "100%", height: "1000px" }}
                />
            </ReactFlowProvider>
        </div>
    );
}
