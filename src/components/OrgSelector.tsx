import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { Theme, useTheme } from "@mui/material/styles";
import { makeStyles } from "@mui/styles";

import React, { Dispatch, SetStateAction } from "react";

import { useAsync } from "../hooks/useAsync";
import { listOrgs } from "../utils/githubApi";

const useStyles = makeStyles(() => ({
    entry: {
        "z-index": 10,
        background: "white",
    },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

function getStyles(org: string, orgs: readonly string[], theme: Theme) {
    return {
        fontWeight:
            orgs.indexOf(org) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

/*
 *
 * A React component that is meant to select an organization.
 *
 * https://mui.com/components/selects/#chip
 */
export default function OrgSelector({
    orgs,
    setOrgs,
}: {
    orgs: string[];
    setOrgs: Dispatch<SetStateAction<string[]>>;
}): JSX.Element {
    const { entry } = useStyles();
    const theme = useTheme();

    const { value } = useAsync<string[]>(() => listOrgs(), []);

    const handleChange = (event: SelectChangeEvent<string[]>) => {
        const {
            target: { value },
        } = event;
        setOrgs(
            // On autofill we get a the stringified value.
            typeof value === "string" ? value.split(",") : value
        );
    };

    return (
        <div className={entry}>
            <FormControl sx={{ m: 1, width: 300 }}>
                <InputLabel id="multiple-chip-in-label">
                    Organizations
                </InputLabel>
                <Select
                    labelId="multiple-chip-label"
                    id="multiple-chip"
                    multiple
                    value={orgs}
                    onChange={handleChange}
                    input={
                        <OutlinedInput
                            id="select-multiple-chip"
                            label="Organizations"
                        />
                    }
                    renderValue={(selected) => (
                        <Box
                            sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}
                        >
                            {selected.map((value) => (
                                <Chip key={value} label={value} />
                            ))}
                        </Box>
                    )}
                    MenuProps={MenuProps}
                >
                    {value === undefined
                        ? []
                        : value.map((org) => (
                              <MenuItem
                                  key={org}
                                  value={org}
                                  style={getStyles(org, orgs, theme)}
                              >
                                  {org}
                              </MenuItem>
                          ))}
                </Select>
            </FormControl>
        </div>
    );
}
