import { Octokit } from "@octokit/core";
import { graphql } from "@octokit/graphql";
import { paginateRest } from "@octokit/plugin-paginate-rest";

const REST_URL = process.env.GITHUB_API_URL || "https://api.github.com";
const GQL_URL = process.env.GITHUB_API_URL || "https://api.github.com/graphql";
const TOKEN = process.env.TOKEN || "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";

const MyOctokit = Octokit.plugin(paginateRest);
const restClient = new MyOctokit({
    baseUrl: REST_URL,
    auth: TOKEN,
});

const gqlClient = graphql.defaults({
    baseUrl: GQL_URL,
    headers: {
        authorization: `token ${TOKEN}`,
    },
});

export type Cards = {
    name: string;
    url: string;
    body: string;
    open: number;
    inProgress: number;
    closed: number;
};

/*
 * List of organizations based on the below API.
 *
 * https://docs.github.com/en/rest/reference/orgs#list-organizations
 *
 * Returns:
 *     A promise for an array of organizations names.
 */
export async function listOrgs(): Promise<string[]> {
    const orgs = await restClient.paginate("GET /organizations", {});

    return orgs.map((org) => org.login);
}

/*
 * Uses the graphql endpoint to get total card counts per column per
 * project in an org.
 *
 * Returns:
 *     A promise for an array of project cards.
 */
export async function listProjectCardsForOrg(org: string): Promise<Cards[]> {
    const { search } = await gqlClient(`{
        search(type:USER, query: "org:${org}", first: 1) {
            edges {
              node {
                ... on Organization {
                  projects(first: 30) {
                    edges {
                      node {
                        name
                        url
                        body
                        columns(first: 30) {
                          edges {
                            node {
                              name
                              cards {
                                totalCount
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }`);
    return search.edges[0].node.projects.edges
        .map((edge) => edge.node)
        .map((node) => {
            const columns: { [name: string]: number } = node.columns.edges
                .map((edge) => edge.node)
                .reduce((acc, column) => {
                    acc[column.name] = column.cards.totalCount;
                    return acc;
                }, {});

            return {
                name: `${org}: ${node.name}`,
                url: node.url,
                body: node.body,
                open: columns["To do"] || 0,
                inProgress: Object.entries(columns)
                    .filter(([name]) => name !== "To do" && name !== "Done")
                    .reduce((acc, [, count]) => acc + count, 0),
                closed: columns["Done"] || 0,
            };
        });
}
