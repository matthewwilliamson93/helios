/*
 * Opens a URL in a new tab.
 *
 * Args:
 *     url: The URL to open in a new tab.
 */
export function openInNewTab(url: string): void {
    window.open(url, "_blank", "noopener,noreferrer");
}
