# Helios

A site to help guide longer term project planning.

https://matthewwilliamson93.gitlab.io/helios/

## Developing

### Development Run

To run the code for local development:

```
make run
```

Runs the app in the development mode.\
Opens [http://localhost:3000](http://localhost:3000) to view.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Production Run

If you are looking to run the the code for release:

```
make prod-run
```

This will build the app in a production mode and serve it from a local NGINX docker image. \
Open [http://localhost](http://localhost) to view the site.

The page will **NOT** reload if you make edits.

### All else

Generally the repo uses a `Makefile` if you want to see
other commands simply run `make`.

```
Usage:
    help:            Prints this screen
    install-deps:    Installs dependencies
    upgrade:         Upgrades the dependencies
    check-fmt:       Checks the formatting of the code
    fmt:             Formats the code
    type-check:      Type checks the code
    lint:            Lints the code
    fix-lint:        Automatically applies certain lint fixes
    build:           Build the frontend for deployment
    run:             Run the frontend in a dev mode
    prod-build:      Build the docker image for the prod mode
    prod-full-build: Build the docker image for the prod mode
    prod-run:        Run the prod mode
    clean:           Clean out temporaries
```

## CI / CD

The repo uses a local gitlab CI config to build the code and deploy it to gitlab pages.

## Repo Layout

```
.
├── craco.config.js
├── deployment
│   ├── app.conf                    - Configuration for the NGINX container
│   └── Dockerfile                  - NGINX container that runs the optimized build from the react app
├── Makefile                        - A proxy for actions that can be run in the repo
├── package.json                    - File to hold all the apps settings
├── public
│   ├── favicon.ico
│   ├── icons
│   │   ├── icon-128.png
│   │   ├── icon-16.png
│   │   ├── icon-24.png
│   │   ├── icon-256.png
│   │   ├── icon-32.png
│   │   ├── icon-48.png
│   │   ├── icon-512.png
│   │   ├── icon-64.png
│   │   ├── icon-72.png
│   │   ├── icon-96.png
│   │   └── icon-maskable.png
│   ├── index.html                  - The entry point for the site
│   ├── manifest.json               - The manifest files is used for PWAs
│   └── robots.txt
├── README.md
├── src
│   ├── App.tsx                     - Holds the main layout of the site
│   ├── components
│   │   ├── OrgSelector.tsx
│   │   ├── ProjctsFlow.tsx
│   │   └── ProjctCard.tsx
│   ├── hooks
│   │   ├── useAsync.ts             - A react hook that loads data based on an async callback
│   │   └── useStickyState.ts       - A react hook that stores the previous value in local storage
│   │                                 for the next run
│   ├── index.tsx                   - Starts the react app
│   ├── react-app-env.d.ts
│   ├── registerServiceWorker.ts    - Handles setting up the service worker
│   └── utils
│       └── githubApi.ts
└── tsconfig.json

```
