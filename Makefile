PACKAGES = node_modules
TAG      = helios

TOOLS     := $(shell yarn bin)
SRC_CODE   = $(shell find src/ -name "*.ts" -o -name "*.tsx" -o -name "*js")
JS_BUNDLES = build/static/js/*.js

.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:            Prints this screen"
	@echo "    install-deps:    Installs dependencies"
	@echo "    upgrade:         Upgrades the dependencies"
	@echo "    anaylze-bundle:  Analyzes the JS bundle"
	@echo "    check-fmt:       Checks the formatting of the code"
	@echo "    fmt:             Formats the code"
	@echo "    type-check:      Type checks the code"
	@echo "    lint:            Lints the code"
	@echo "    fix-lint:        Automatically applies certain lint fixes"
	@echo "    build:           Build the frontend for deployment"
	@echo "    run:             Run the frontend in a dev mode"
	@echo "    prod-build:      Build the docker image for the prod mode"
	@echo "    prod-full-build: Build the docker image for the prod mode"
	@echo "    prod-run:        Run the prod mode"
	@echo "    clean:           Clean out temporaries"
	@echo ""

$(PACKAGES):
	@echo "Installing local dependencies"
	yarn install --ignore-optional --ignore-engines

.PHONY:
install-deps: $(PACKAGES)

.PHONY: upgrade
upgrade: $(PACKAGES)
	@echo "Upgrading dependencies"
	yarn upgrade-interactive --latest

.PHONY: anaylze-bundle
anaylze-bundle: build
	@echo "Analyzing the JS bundle"
	$(TOOLS)/source-map-explorer --no-border-checks $(JS_BUNDLES)

.PHONY: check-fmt
check-fmt: $(PACKAGES)
	@echo "Checking the formatting of the code"
	$(TOOLS)/prettier --check $(SRC_CODE)

.PHONY: fmt
fmt: $(PACKAGES)
	@echo "Auto Formatting"
	$(TOOLS)/prettier --write $(SRC_CODE)

.PHONY: lint
lint: $(PACKAGES)
	@echo "Type Checking & Linting"
	$(TOOLS)/tsc --project tsconfig.json
	$(TOOLS)/eslint $(SRC_CODE)

.PHONY: fix-lint
fix-lint: $(PACKAGES)
	@echo "Auto fixing lints"
	$(TOOLS)/eslint --fix src/

.PHONY: build
build: $(PACKAGES)
	@echo "Building the frontend"
	yarn build

.PHONY: run
run: $(PACKAGES)
	@echo "Serving site in WATCH/DEV"
	yarn run start

.PHONY: prod-build
prod-build:
	@echo "Building the production image"
	docker build -f deployment/Dockerfile -t $(TAG) .

.PHONY: prod-full-build
prod-full-build:
	@echo "Building the production image and checking if base images need to be updated"
	docker build --pull -f deployment/Dockerfile -t $(TAG) .

.PHONY: prod-run
prod-run: prod-build
	@echo "Serving the site from NGINX/PROD"
	docker run --rm -p 80:80 $(TAG)

.PHONY: clean
clean:
	@echo "Removing temporary files"
	rm -rf node_modules/ build/
